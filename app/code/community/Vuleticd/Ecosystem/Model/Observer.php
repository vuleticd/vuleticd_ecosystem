<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 class Vuleticd_Ecosystem_Model_Observer
 {
 	// queueing of changes on source system
 	public function saveModelAfter($observer)
 	{
 		//$time_start = microtime(true);
 		$helper = Mage::helper('ecosystem'); 
 		try {
           	$targets = $helper->getTargets();
           	$entities = $helper->getEntities(true);
           	if (!$targets || !$entities) {
	 			return;
	 		}
	 		$object = $observer->getEvent()->getObject();
	 		$objectName = get_class($object);
	 		$objectTaxonomy = array_merge(
	 			array($objectName => $objectName),
	 			class_parents($object)
	 		);
	 		// Making sure that this works even of Mage core model is rewritten, as long as it has Core class as parent
 			$entityType = current(array_intersect($objectTaxonomy, $entities));
 			$job = Mage::getModel('ecosystem/job', array( 'entity_type' => $entityType ));
 			if ($entityType) {
 				foreach ($targets as $target) {
	                $config = $target->getConfig();
	                if ('disabled' != $config['mode'][$entityType]) {
	                	$helper->debug('OBSERVER::START ' . $entityType . '::'  . ' on Target "' . $target->getName() . '".');
	 				   	$profile = Mage::getModel('ecosystem/profile', array( 'entity_object' => $object ))->init($job, $target);
	 				   	$profile->push();
	 				   	$helper->debug('OBSERVER::FINISH ' . $entityType . '::' . $profile->getEntityAdapter()->getIdentifier() . ' on Target "' . $target->getName() . '".');
	                }
	 			}
 			}
        } catch (Exception $e) {
            $helper->debug($e->getMessage(), Zend_Log::ERR);
        }
        //Mage::log($objectName . '::Total execution time in seconds: ' . (microtime(true) - $time_start));
 	}
 }