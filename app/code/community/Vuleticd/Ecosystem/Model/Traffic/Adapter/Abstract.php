<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @method void setForced(boolean $true)
 * @method boolean getForced()
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract extends Varien_Object
{
    protected $_cli;
    protected $_cli_session;

    // Execute the call to remote SOAP API and return the result
    public function call($remote, $endpoint, $params)
    {
        if ($params instanceof SoapVar) {
            $params = array($params);
        }

        try {
            $this->_getClient($remote);
            $result = $this->_cli->call($this->_cli_session, $endpoint, $params);
            return $result;
        } catch (SoapFault $fault) {
            throw new Exception("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})");
        } catch (Exception $e) {
            throw $e;
        }
    }

    // initiate the remote SOAP API connection
    protected function _getClient($remote)
    {
        if ($this->_cli_session) {
            return $this->_cli_session;
        }

        if (!$remote) {
            // throw error
            return null;
        }

        $soapUrl = rtrim($remote->getBaseUrl(), '/') . '/api/soap/?wsdl';
        try {
            $this->_cli = new SoapClient($soapUrl);
            $this->_cli_session = $this->_cli->login($remote->getApiUser(), $remote->getApiPass());

            return $this->_cli_session;
        } catch (SoapFault $fault) {
            throw new Exception("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})");
        } catch (Exception $e) {
            throw $e;
        }
    }

    // push notification signal to remote target
    public function push($job, $remote, $entityAdapter)
    {
        $params =new SoapVar(
            array(
                'type' => $job->getEntityType(),
                'identifier' => $job->getEntityIdentifier(),
                'url'   => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
            ), 
            SOAP_ENC_OBJECT
        );

        $result = $this->call($remote, 'ecosystem.push', $params);
        Mage::helper('ecosystem')->debug('PUSH::ABSTRACT ');
    }

    // pull data from remote source and process it on target, after signal is received
    public function pull($job, $remote, $entityAdapter)
    {
        $identity = explode('/', $job->getEntityIdentifier());
        $oldIdentifier = $identity[0];
        $newIdentifier = $identity[1];
        $config = $remote->getConfig();
        /* request new identifier data from source */
        $result = $this->call($remote, $config['endpoint'][$job->getEntityType()], $newIdentifier);
        Mage::helper('ecosystem')->debug('PULL::ABSTRACT ');
        // use when pulling to load the entity which needs to be updated
        $objectAlias = Mage::helper('ecosystem')->getObjectAlias($job->getEntityType());
        if ($oldIdentifier && $oldIdentifier != $newIdentifier) {
            $identifier = $oldIdentifier;
        } else {
            $identifier = $newIdentifier;
        }

        $object = $entityAdapter->loadObject($objectAlias, $identifier);
        if ($object) {
            $entityAdapter->setEntityObject($object);
        }

        $data = $entityAdapter->filter($result, explode(',', $config['ignore'][$job->getEntityType()]));
        if (! empty($data)) {
            $entityAdapter->save($data);
        }
    }

}