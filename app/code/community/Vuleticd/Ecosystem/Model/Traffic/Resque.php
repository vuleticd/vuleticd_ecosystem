<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Model_Traffic_Resque extends Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract
{

    public function push($job, $remote, $entityAdapter)
    {
        $helper = Mage::helper('ecosystem');
        try {
            $host = (string)Mage::getConfig()->getNode('ecosystem/redis/host');
            $port = (string)Mage::getConfig()->getNode('ecosystem/redis/port');
            $db = (string)Mage::getConfig()->getNode('ecosystem/redis/db');
            Resque::setBackend($host.':'.$port, $db);
            $args = array(
                    'type' => $job->getEntityType(),
                    'identifier' => $entityAdapter->getIdentifier(),
                    'url'   => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
                );
            $jobId = Resque::enqueue($helper->getQueueName($remote), get_class($this), $args);
            $helper->debug('PUSH::RESQUE ' . $jobId);
        } catch (Exception $e) {
            throw $e;
        }
    }

    // ... Set up environment for this job
    public function setUp()
    {
        
    }
    
    public function perform()
    {
        $helper = Mage::helper('ecosystem');
        try {
            // Work work work
            $job = Mage::getModel('ecosystem/job', array( 'entity_type' => $this->args['type'], 'entity_identifier' => $this->args['identifier'] ));
            $source = $helper->isSubscribed($this->args['url']);
            if (!$source) {
                throw new Exception('not_subscribed');
            } 
            if ($job->getEntityType()) {
                $helper->debug('RESQUE::START ' . $this->args['type'] . '::' . $this->args['identifier'] . ' from Source "' . $source->getName() . '".');
                $profile = Mage::getModel('ecosystem/profile')->init($job, $source);
                $profile->pull(true);
                $helper->debug('RESQUE::FINISH ' . $this->args['type'] . '::' . $this->args['identifier'] . ' from Source "' . $source->getName() . '".');
            }
        } catch (Exception $e) {
            $helper->debug($e->getMessage(), Zend_Log::ERR);
            throw $e;
        }
    }
    
    // ... Remove environment for this job
    public function tearDown()
    {
    
    }
}