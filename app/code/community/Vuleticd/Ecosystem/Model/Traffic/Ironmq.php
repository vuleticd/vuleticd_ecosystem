<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Model_Traffic_Ironmq extends Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract
{
    protected $_ironmq;

    public function push($job, $remote, $entityAdapter)
    {
        $helper = Mage::helper('ecosystem');
        try {
            $args = array(
                    'type' => $job->getEntityType(),
                    'identifier' => $entityAdapter->getIdentifier(),
                    'url'   => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
                );

            $ironJob = $this->getIronClient()->postMessage(
                            $helper->getQueueName($remote),
                            json_encode($args), 
                            array(
                                "timeout" => 60,
                                "delay" => 0,
                                "expires_in" => 2*24*3600
                            ));

            $helper->debug('PUSH::IRONMQ ' . $ironJob->id);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getIronClient()
    {
        if ($this->_ironmq) {
            return $this->_ironmq;
        }
        
        try {
            $this->_ironmq = new Ironmq_Client(array(
                "token" => (string)Mage::getConfig()->getNode('ecosystem/ironmq/token'),
                "project_id" => (string)Mage::getConfig()->getNode('ecosystem/ironmq/project')
            ));
            $this->_ironmq->ssl_verifypeer = false;

            return $this->_ironmq;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function perform()
    {
        $helper = Mage::helper('ecosystem');
        try {

            $message = json_decode($this->args->body);
            $job = Mage::getModel('ecosystem/job', array( 'entity_type' => $message->type, 'entity_identifier' => $message->identifier ));
            $source = $helper->isSubscribed($message->url);
            if (!$source) {
                throw new Exception('not_subscribed');
            }

            if ($job->getEntityType()) {
                $helper->debug('IRONMQ::START ' . $message->type . '::' . $message->identifier . ' from Source "' . $source->getName() . '".');
                $profile = Mage::getModel('ecosystem/profile')->init($job, $source);
                $profile->pull(true);
                $helper->debug('IRONMQ::FINISH ' . $message->type . '::' . $message->identifier . ' from Source "' . $source->getName() . '".');
            }

            $this->getIronClient()->deleteMessage($helper->getQueueName(), $this->args->id);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function fail(Exception $e)
    {
        Mage::helper('ecosystem')->debug($e->getMessage(), Zend_Log::ERR);
    }
    
}