<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Model_Traffic_Mysql_Queue extends Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract
{
    // when forced push notification signal to remote target, othervise queue the signal for pushing with cron
    public function push($job, $remote, $entityAdapter)
    {
        $helper = Mage::helper('ecosystem');
        if ($this->getForced()) {
            parent::push($job, $remote, $entityAdapter);
        } else {
            try {
                $model = Mage::getModel('ecosystem/queue_source');
                $model->setTargetId($remote->getId());
                $model->setEntityIdentifier($entityAdapter->getIdentifier());
                $model->setEntityType($job->getEntityType());
                $model->save();
                $helper->debug('PUSH::QUEUE ' . $model->getId());
            } catch (Exception $e) {
                // don't queue jobs which are already in queue
                if (strpos($e->getMessage(), 'UNQ_ECOSYSTEM_QUEUE_SOURCE_ENTT_TYPE_ENTT_IDENTIFIER_TARGET_ID') !== false) {
                    $helper->debug('DUPLICATE::QUEUE ' . $e->getMessage());
                } else {
                    throw $e;
                }
            }
        }
    }

    // when forced pull data from remote source and process it on target, othervise queue the job for pulling with cron
    public function pull($job, $remote, $entityAdapter)
    {
        $helper = Mage::helper('ecosystem');
        if ($this->getForced()) {
            parent::pull($job, $remote, $entityAdapter);
        } else {
            try {
                $model = Mage::getModel('ecosystem/queue_target');
                $model->setSourceId($remote->getId());
                $model->setEntityIdentifier($job->getEntityIdentifier());
                $model->setEntityType($job->getEntityType());
                $queueId = $model->save();
                $helper->debug('PULL::QUEUE ' . $model->getId());
            } catch (Exception $e) {
                // don't queue jobs which are already in queue
                if (strpos($e->getMessage(), 'UNQ_ECOSYSTEM_QUEUE_TARGET_ENTT_TYPE_ENTT_IDENTIFIER_SOURCE_ID') !== false) {
                    $helper->debug('DUPLICATE::QUEUE ' . $e->getMessage());
                } else {
                    throw $e;
                }
            }
        }
    }
}