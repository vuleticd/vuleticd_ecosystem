<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 abstract class Vuleticd_Ecosystem_Model_Entity_Adapter_Abstract extends Varien_Object
 {
    protected $_id_key = 'id';

    public function loadObject($objectAlias, $identifier)
    {
        $model = Mage::getModel($objectAlias)->load($identifier);
        if (!$model) {
            throw new Exception('Invalid model loaded.');
            return false;
        }
        Mage::helper('ecosystem')->debug('LOAD::ABSTRACT');
        return $model;
    }

 	// retreive unique identifier which will be used between source and target, to identify entity
 	public function getIdentifier()
 	{
 		return $this->getEntityObject()->getOrigData($this->_id_key) .'/'. $this->getEntityObject()->getData($this->_id_key);
 	}

    // save filtered entity object on target side
 	public function save($data)
 	{
 		$this->getEntityObject()->addData($data);
        try {
            $this->getEntityObject()->save();
            Mage::helper('ecosystem')->debug('SAVE::ABSTRACT');
        } catch (Exception $e) {
            throw $e;
        }
 	}

    // filter pull results before saving on target side
    public function filter($data, $ignores)
    {
        foreach ((array) $ignores as $ignore) {
            unset($data[$ignore]);
        }
        Mage::helper('ecosystem')->debug('FILTER::ABSTRACT');
        return $data;
    }
 }