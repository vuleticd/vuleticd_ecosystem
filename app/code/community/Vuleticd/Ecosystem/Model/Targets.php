<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Model_Targets extends Mage_Core_Model_Abstract
{
	protected $_eventPrefix = 'ecosystem_targets';
    protected $_cacheTag    = true;

    protected function _construct()
    {
        $this->_init("ecosystem/targets");
    }

    protected function _afterSave()
    {
        $this->setConfig(array(
        		'mode'	=> $this->getMode()
        	));
        $this->unsetData('mode');
        $config = $this->getConfig();
        if ($config) {
        	Mage::getModel('core/config')->saveConfig('ecosystem/targets/id' . $this->getId(), serialize($config));
        }

        parent::_afterSave();
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        $config = Mage::helper('ecosystem')->getConfig('ecosystem/targets/id' . $this->getId());
        // get configuration from DB or config files
        // set configuration as model data
        $this->setConfig($config);
    }
}