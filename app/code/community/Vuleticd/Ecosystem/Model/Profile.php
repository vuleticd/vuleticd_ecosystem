<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @method Varien_Object || Vuleticd_Ecosystem_Model_Queue_Target || Vuleticd_Ecosystem_Model_Queue_Source  setJob(object $job)
 * @method Varien_Object || Vuleticd_Ecosystem_Model_Queue_Target || Vuleticd_Ecosystem_Model_Queue_Source getJob()
 * @method Vuleticd_Ecosystem_Model_Entity_Adapter_Abstract setEntityAdapter(instanceof Vuleticd_Ecosystem_Model_Entity_Adapter_Abstract)
 * @method instanceof Vuleticd_Ecosystem_Model_Entity_Adapter_Abstract getEntityAdapter()
 * @method Vuleticd_Ecosystem_Model_Sources || Vuleticd_Ecosystem_Model_Targets setRemote(object $remote)
 * @method Vuleticd_Ecosystem_Model_Sources || Vuleticd_Ecosystem_Model_Targets getRemote()
 * @method void setTrafficAdapterClass(string $value)
 * @method string getTrafficAdapterClass()
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 class Vuleticd_Ecosystem_Model_Profile extends Varien_Object
 {
 	public function init($job, $remote = null)
 	{
        $helper = Mage::helper('ecosystem');
        $this->setJob($job);
        // setting entity
        $entityClass = Mage::getConfig()->getNode('ecosystem/entities/' . $this->getJob()->getEntityType())->adapter;
        if (!$entityClass) {
            throw new Exception('Invalid Entity class::' . $entityClass);
            return $this;
        }
        $entityAdapterObject = Mage::getModel($entityClass, array( 'entity_object' => $this->getEntityObject()));
        if (!$entityAdapterObject instanceof Vuleticd_Ecosystem_Model_Entity_Adapter_Abstract) {
            throw new Exception('Entity adapter class ' . $entityClass . 'must extend Vuleticd_Ecosystem_Model_Entity_Adapter_Abstract');
            return $this;
        }
        $this->setEntityAdapter($entityAdapterObject);
        $helper->debug('SET::ENTITY ' . Mage::getConfig()->getModelClassName($entityClass));
        // setting remote
        if (!$remote) {
            if ($this->getJob()->getTargetId()) {
                $remote = Mage::getModel('ecosystem/targets')->load($this->getJob()->getTargetId());
            } elseif ($this->getJob()->getSourceId()) {
                $remote = Mage::getModel('ecosystem/sources')->load($this->getJob()->getSourceId());
            }
        }
        if (get_class($remote) != 'Vuleticd_Ecosystem_Model_Targets' && get_class($remote) != 'Vuleticd_Ecosystem_Model_Sources') {
            throw new Exception('Invalid remote class ' . get_class($remote) );
            return $this;
        }
        $this->setRemote($remote);
        $helper->debug('SET::REMOTE ' . $remote->getName());
        $config = $this->getRemote()->getConfig();
        if ('disabled' == $config['mode'][$this->getJob()->getEntityType()]) {
            throw new Exception('DISABLED Entity::' . $this->getJob()->getEntityType());
            return $this;
        }

        $trafficAdapterClass = (string) Mage::getConfig()->getNode('ecosystem/traffic/adapter/' . $config['mode'][$this->getJob()->getEntityType()]);
        if (!$trafficAdapterClass || !class_exists($trafficAdapterClass)) {
            throw new Exception('Invalid Traffic adapter class::' . $trafficAdapterClass);
            return $this;
        }
        $this->setTrafficAdapterClass($trafficAdapterClass);
        $helper->debug('SET::TRAFFIC ' . $trafficAdapterClass);
 		return $this;
 	}

    /*
     * push notifications to target, queue or realtime is forced or decided by configuration
     */
    public function push($force = false)
    {
        $traficAdapter = Mage::getModel($this->getTrafficAdapterClass(), array( 'forced' => $force ));
        if (!$traficAdapter instanceof Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract) {
            throw new Exception('Entity adapter class ' . $this->getTrafficAdapterClass() . 'must extend Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract');
            return;
        }
        $traficAdapter->push($this->getJob(), $this->getRemote(), $this->getEntityAdapter());
    }

    /*
     * pull data from source method, queue or realtime is forced or decided by configuration
     */
    public function pull($force = false)
    {
        $traficAdapter = Mage::getModel($this->getTrafficAdapterClass(), array( 'forced' => $force ));
        if (!$traficAdapter instanceof Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract) {
            throw new Exception('Entity adapter class ' . $this->getTrafficAdapterClass() . 'must extend Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract');
            return;
        }
        $traficAdapter->pull($this->getJob(), $this->getRemote(), $this->getEntityAdapter());
    }
 }