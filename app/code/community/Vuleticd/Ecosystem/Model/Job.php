<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @method void  setEntityType(string $entity_type)
 * @method string getEntityType()
 * @method void  setDirection(string push|pull)
 * @method string getDirection()
 * @method void  setId(int $jobId)
 * @method int getId()
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 class Vuleticd_Ecosystem_Model_Job extends Varien_Object
 {
 	protected $_db_job = null;

 	protected function _construct()
    {
    	if (!$this->getEntityType()) {
    		$this->_db_job = null;
    		switch ($this->getDirection()) {
    			case 'push':
    				if ($this->getId()) {
    					$this->_db_job = Mage::getModel('ecosystem/queue_source')->load($this->getId());
    				} else {
    					$this->_db_job = Mage::getModel('ecosystem/queue_source')->getCollection()->setPageSize(1)->getFirstItem();
    				}
    				break;
    			
    			case 'pull':
    				if ($this->getId()) {
    					$this->_db_job = Mage::getModel('ecosystem/queue_target')->load($this->getId());
    				} else {
    				 	$this->_db_job = Mage::getModel('ecosystem/queue_target')->getCollection()->setPageSize(1)->getFirstItem();
    				}

    				break;

    		}
    		if ($this->_db_job) {
    			$this->setData($this->_db_job->getData());
    		}
    	}
    }

    public function delete()
    {
    	if ($this->_db_job) {
    		$this->_db_job->delete();
    	}
    }
 }