<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Block_Adminhtml_Targets_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'ecosystem';
        $this->_controller = 'adminhtml_targets';

        parent::__construct();

        if (Mage::helper('ecosystem/admin')->isActionAllowed('targets/save')) {
            $this->_updateButton('save', 'label', Mage::helper('ecosystem')->__('Save Target'));
            $this->_addButton('saveandcontinue', array(
                'label' => Mage::helper('adminhtml')->__('Save and Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save',
            ), -100);
        } else {
            $this->_removeButton('save');
        }

        if (Mage::helper('ecosystem/admin')->isActionAllowed('targets/delete')) {
            $this->_updateButton('delete', 'label', Mage::helper('ecosystem')->__('Delete Target'));
        } else {
            $this->_removeButton('delete');
        }

        $this -> _formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'page_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'page_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        $model = Mage::helper('ecosystem/admin')->getTargetInstance();
        if ($model->getId()) {
            return Mage::helper('ecosystem')->__("Edit Target '%s'", $this->escapeHtml($model->getName()));
        } else {
            return Mage::helper('ecosystem')->__('New Target');
        }
    }

}