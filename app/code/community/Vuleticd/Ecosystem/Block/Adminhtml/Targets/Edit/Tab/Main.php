<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Block_Adminhtml_Targets_Edit_Tab_Main
extends Mage_Adminhtml_Block_Widget_Form
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {
        $model = Mage::helper('ecosystem/admin')->getTargetInstance();

        /**
         * Checking if user have permissions to save information
         */
        if (Mage::helper('ecosystem/admin')->isActionAllowed('targets/save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('target_main_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('ecosystem')->__('Connection Details')));

        // Form data
       
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array('name' => 'id'));
        }

        $fieldset->addField('enabled', 'select', array(
        	'name'      => 'enabled',
	        'label'     => Mage::helper('ecosystem')->__('Enabled'),
	        'title'     => Mage::helper('ecosystem')->__('Enabled'),
	        'required'  => true,
	        'values' => array('0' => 'No', '1' => 'Yes'),
	        'disabled' => $isElementDisabled
        ));

        $fieldset->addField('name', 'text', array(
            'name' => 'name',
            'label' => Mage::helper('ecosystem')->__('Target Name'),
            'title' => Mage::helper('ecosystem')->__('Target Name'),
            'required' => true,
            'disabled' => $isElementDisabled
        ));

        $fieldset->addField('base_url', 'text', array(
            'name' => 'base_url',
            'label' => Mage::helper('ecosystem')->__('Base Url'),
            'title' => Mage::helper('ecosystem')->__('Base Url'),
            'required' => true,
            'disabled' => $isElementDisabled
        ));

        $fieldset->addField('api_user', 'text', array(
            'name' => 'api_user',
            'label' => Mage::helper('ecosystem')->__('API username'),
            'title' => Mage::helper('ecosystem')->__('API username'),
            'required' => true,
            'disabled' => $isElementDisabled
        ));

        $fieldset->addField('api_pass', 'password', array(
            'name' => 'api_pass',
            'label' => Mage::helper('ecosystem')->__('API password'),
            'title' => Mage::helper('ecosystem')->__('API password'),
            'required' => true,
            'disabled' => $isElementDisabled
        ));

        Mage::dispatchEvent('adminhtml_target_edit_tab_main_prepare_form', 
        	array('form' => $form));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('ecosystem') -> __('General');
    }

    public function getTabTitle()
    {
        return Mage::helper('ecosystem') -> __('General');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
