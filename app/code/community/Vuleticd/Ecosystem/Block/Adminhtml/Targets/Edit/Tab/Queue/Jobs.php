<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_Banner
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

class Vuleticd_Ecosystem_Block_Adminhtml_Targets_Edit_Tab_Queue_Jobs extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('targets_edit_tab_queue_jobs_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
        /*
        $this->setVarNameFilter('related_salesrule_filter');
        if ($this->_getBanner()->getId()) {
            $this->setDefaultFilter(array('in_banner_salesrule'=>1));
        }
        */
    }

    protected function _prepareCollection()
    {
        $targetId = Mage::registry('target_item')->getId();
        $collection = Mage::getModel('ecosystem/queue_source')->getCollection()
            ->addFieldToFilter('target_id', $targetId);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Create grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('ecosystem')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'id',
            'filter' => false
        ));

        $this->addColumn('entity_type', array(
            'header'    => Mage::helper('ecosystem')->__('Entity'),
            'align'     =>'left',
            'index'     => 'entity_type',
        ));

        $this->addColumn('entity_identifier', array(
            'header'    => Mage::helper('ecosystem')->__('Entity Identifier'),
            'align'     =>'left',
            'index'     => 'entity_identifier',
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('ecosystem')->__('Action'),
            'width' => '100px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array( array(
                    'caption' => Mage::helper('ecosystem')->__('Send'),
                    'url'       => '#',
                    'onclick'   => 'return targetsControl.processItem($id);'
                )),
            'filter' => false,
            'sortable' => false,
            'index' => 'id',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/queueJobsGrid', array('_current'=>true));
    }

    protected function _getTarget()
    {
        return Mage::registry('target_item');
    }
}
