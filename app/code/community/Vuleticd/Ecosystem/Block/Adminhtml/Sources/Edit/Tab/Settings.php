<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Block_Adminhtml_Sources_Edit_Tab_Settings extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $model = Mage::helper('ecosystem/admin')->getSourceInstance();

        /**
         * Checking if user have permissions to save information
         */
        if (Mage::helper('ecosystem/admin')->isActionAllowed('sources/save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('source_settings_');

        $entities = Mage::helper('ecosystem')->getEntities();
        foreach ($entities as $entityType => $entityConfiguration) {
            $fieldset = $form->addFieldset('settings_'.$entityType.'_fieldset', array('legend' => Mage::helper('ecosystem')->__($entityType . ' Settings')));
        
            $fieldset->addField($entityType .'_mode', 'select', array(
                'name'      => 'mode[' . $entityType . ']',
                'label'     => Mage::helper('ecosystem')->__('Pull Mode'),
                'title'     => Mage::helper('ecosystem')->__('Pull Mode'),
                'required'  => true,
                'values' => Mage::getModel('ecosystem/traffic_options')->toOptionArray()
            ));
        }
        
        Mage::dispatchEvent('adminhtml_source_edit_tab_settings_prepare_form', 
            array('form' => $form));

        $form->setValues($model->getData());
        foreach ($model->getConfig() as $fieldName => $values) {
            if ($fieldName != 'mode') {
                continue;
            }
            foreach ($values as $entType => $value) {
                $form->getElement($entType .'_mode')->setValue($value);
            }
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return Mage::helper('ecosystem')->__('Settings');
    }

    public function getTabTitle()
    {
        return Mage::helper('ecosystem')->__('Settings');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}