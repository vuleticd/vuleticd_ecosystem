<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Block_Adminhtml_Sources_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sourcesGrid');
        $this->setDefaultSort('enabled');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ecosystem/sources')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('ecosystem')->__('ID'),
            'width' => '50px',
            'index' => 'id'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('ecosystem')->__('Source'),
            'index' => 'name'
        ));
        $this->addColumn('base_url', array(
            'header' => Mage::helper('ecosystem')->__('Base Url'),
            'index' => 'base_url',
        ));

        $this->addColumn('enabled', array(
            'header' => Mage::helper('ecosystem')->__('Enabled'),
            'index' => 'enabled'
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('ecosystem')->__('Action'),
            'width' => '100px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array( array(
                    'caption' => Mage::helper('ecosystem')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )),
            'filter' => false,
            'sortable' => false,
            'index' => 'source',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('source');
        if (Mage::helper('ecosystem/admin')->isActionAllowed('sources/save')) {
            $this->getMassactionBlock()->addItem('enable', array(
                'label' => Mage::helper('ecosystem')->__('Enable'),
                'url' => $this->getUrl('*/*/massEnable')
            ));
            $this->getMassactionBlock()->addItem('disable', array(
                'label' => Mage::helper('ecosystem')->__('Disable'),
                'url' => $this->getUrl('*/*/massDisable')
            ));
        }
        if (Mage::helper('ecosystem/admin')->isActionAllowed('sources/delete')) {
            $this->getMassactionBlock()->addItem('delete', array(
                'label' => Mage::helper('ecosystem')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('ecosystem')->__('Are you sure?')
            ));
        }

        return $this;
    }

}