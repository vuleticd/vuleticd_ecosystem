<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 class Vuleticd_Ecosystem_Helper_Data extends Mage_Core_Helper_Abstract
 {
 	public function hasTargets()
 	{
 		return Mage::getModel('ecosystem/targets')->getCollection()->addFieldToFilter('enabled',1)->getSize();
 	}

    // enabled sources available
 	public function hasSources()
 	{
 		return Mage::getModel('ecosystem/sources')->getCollection()->addFieldToFilter('enabled',1)->getSize();
 	}

    /*
     * get all enabled target application models or false if nothing is enabled
     */
 	public function getTargets()
 	{
 		// should not be sending to any targets
 		if (!$this->hasTargets()) {
            return false;
        }

        return Mage::getModel('ecosystem/targets')->getCollection()->addFieldToFilter('enabled',1);
 	}

    /*
     * get source application model with passed $url or false if $url is not subscribed or enabled 
     */
 	public function isSubscribed($url)
 	{
        $url = rtrim($url, '/');
 		// nothing should be calling this endpoint
 		if (!$this->hasSources()) {
            return false;
        }

        if (!$url) {
            $helper->debug('URL ' . $url . ' is invalid.');
            return false;
        }

        // this URL should be calling this endpoint
        $source = Mage::getModel('ecosystem/sources')->getCollection()
                            ->addFieldToFilter('base_url',$url)
                            ->addFieldToFilter('enabled',1)
                            ->setPageSize(1)
                            ->getFirstItem();
        if (!$source) {
        	return false;
        }

        return $source;
 	}

    public function getEntities($keysOnly = false)
    {
        $entityTypes = array();
        foreach (Mage::getConfig()->getNode('ecosystem/entities')->children() as $entityType => $entityNode) {
            if ((int)$entityNode->active) {
                $entityTypes[$entityType] = $entityNode;
            } else {
                $this->debug('Entity "' . $entityType . '"" is disabled.');
            }
        }
        return $keysOnly ? array_keys($entityTypes) : $entityTypes;
    }

    public function getObjectAlias($entityType)
    {
        $explode = explode('_Model_',$entityType);
        $m = Mage::getConfig()->getXpath('global/models/*');
        foreach ($m as $node) {
            if ($node->class == $explode[0] . '_Model') {
                return $node->getName() . '/' . strtolower($explode[1]);
            }
        }
        return false;
    }

    public function getConfig($path)
    {
        $config = array();
        $xml = array();
        if (strpos($path,'/targets/') !== false) {
            $direction = 'push';
        } else {
            $direction = 'pull';
        }
        $db = Mage::getStoreConfig($path);
        if ($db) {
            $config = unserialize($db);
        }

        foreach ($this->getEntities(true) as $type) {
            $xml['mode'][$type] = (string)Mage::getConfig()->getNode('ecosystem/entities/' .$type. '/'.$direction.'/mode');
            if ('pull' == $direction) {
                $ignores = array();
                $xml['endpoint'][$type] = (string)Mage::getConfig()->getNode('ecosystem/entities/' .$type. '/'.$direction.'/endpoint');
                if (Mage::getConfig()->getNode('ecosystem/entities/' . $type)->pull->ignore) {
                    foreach (Mage::getConfig()->getNode('ecosystem/entities/' . $type)->pull->ignore->children() as $key=>$value) {
                        $ignores[] = $key;
                    }
                }
                $xml['ignore'][$type] = implode(',', $ignores);
            }
        }

        $merged = array_merge($xml, $config);
        return $merged;
    }

    public function getQueueName($target = null)
    {
        $url =  $target ? $target->getBaseUrl() : Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $queueName = rtrim($url, '/');
        $parsed = parse_url($queueName);
        unset($parsed['scheme']);
        $queueName = $this->_unparseUrl($parsed);

        return $queueName;
    }

    protected function _unparseUrl($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    public function debug($debugData, $level = null)
    {
        if (Mage::getStoreConfigFlag('ecosystem/debug/enabled')) {
            Mage::log($debugData, $level, 'ecosystem.log', true);
        }
    }
 }