<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 class Vuleticd_Ecosystem_Helper_Admin extends Mage_Core_Helper_Abstract
 {
 	protected $_sourceInstance;
 	protected $_targetInstance;

 	public function isActionAllowed($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('ecosystem/' . $action);
    }

    public function getSourceInstance()
    {
        if (!$this->_sourceInstance) {
            $this->_sourceInstance = Mage::registry('source_item');
            if (!$this->_sourceInstance) {
                Mage::throwException($this->__('Source instance does not exist in Registry'));
            }
        }

        return $this->_sourceInstance;
    }

    public function getTargetInstance()
    {
        if (!$this->_targetInstance) {
            $this->_targetInstance = Mage::registry('target_item');
            if (!$this->_targetInstance) {
                Mage::throwException($this->__('Target instance does not exist in Registry'));
            }
        }

        return $this->_targetInstance;
    }
 }