<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

/*
 * ecosystem_sources DB table
 */
if (!$installer->tableExists($installer->getTable('ecosystem/sources'))) {
    $table = $installer->getConnection()
        ->newTable($this->getTable('ecosystem/sources'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Source Id')
        ->addColumn('base_url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        ), 'Source Base Url')
        ->addColumn('api_user', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'API username for Source API')
        ->addColumn('api_pass', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'API password for Source API')
        ->addIndex(
            $installer->getIdxName(
                'ecosystem/sources', 
                array('base_url'), 
                Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('base_url'), 
            array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE));

    $installer->getConnection()->createTable($table);
}

/*
 * ecosystem_queue_source DB table
 */
if (!$installer->tableExists($installer->getTable('ecosystem/queue_source'))) {
    $table = $installer->getConnection()
        ->newTable($this->getTable('ecosystem/queue_source'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Update Id')
        ->addColumn('entity_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'Updated Entity Type')
        ->addColumn('entity_identifier', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'Unique Entity Identifier')
        ->addColumn('target_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        	'unsigned'  => true,
            'nullable'  => false
        ), 'Target ID')
        ->addForeignKey(
            $installer->getFkName(
                'ecosystem/queue_source', 
                'target_id', 
                'ecosystem/targets', 
                'id'),
            'target_id', 
            $installer->getTable('ecosystem/targets'), 
            'id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

    $installer->getConnection()->createTable($table);
}

/*
 * ecosystem_targets DB table
 */
if (!$installer->tableExists($installer->getTable('ecosystem/targets'))) {
    $table = $installer->getConnection()
        ->newTable($this->getTable('ecosystem/targets'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Target Id')
        ->addColumn('base_url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        ), 'Target Base Url')
        ->addColumn('api_user', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'API username for Target API')
        ->addColumn('api_pass', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'API password for Target API')
        ->addIndex(
            $installer->getIdxName(
                'ecosystem/targets', 
                array('base_url'), 
                Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('base_url'), 
            array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE));

    $installer->getConnection()->createTable($table);
}


/*
 * ecosystem_queue_target DB table
 */
if (!$installer->tableExists($installer->getTable('ecosystem/queue_target'))) {
    $table = $installer->getConnection()
        ->newTable($this->getTable('ecosystem/queue_target'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'Update Id')
        ->addColumn('entity_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'Updated Entity Type')
        ->addColumn('entity_identifier', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'Unique Entity Identifier')
        ->addColumn('source_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        	'unsigned'  => true,
            'nullable'  => false
        ), 'Source ID')
        ->addForeignKey(
            $installer->getFkName(
                'ecosystem/queue_target', 
                'source_id', 
                'ecosystem/sources', 
                'id'),
            'source_id', 
            $installer->getTable('ecosystem/sources'), 
            'id',
            Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

    $installer->getConnection()->createTable($table);
}

$installer->endSetup();