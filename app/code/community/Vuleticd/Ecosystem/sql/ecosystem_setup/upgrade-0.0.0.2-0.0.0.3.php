<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

if ($installer->tableExists($installer->getTable('ecosystem/targets'))) {
	$installer->getConnection()
	    ->addColumn($installer->getTable('ecosystem/targets'), 'enabled', array(
	    	'unsigned'  => true,
        	'default'   => '0',
	        'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
	        'comment' => 'Enabled Target'
	    	));

	$installer->getConnection()
	    ->addColumn($installer->getTable('ecosystem/targets'), 'name', array(
	    	'nullable' => false,
            'length' => 255,
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'comment' => 'Target Description'
	    	));
}
$installer->endSetup();