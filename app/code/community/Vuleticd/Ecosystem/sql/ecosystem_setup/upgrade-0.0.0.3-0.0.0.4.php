<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

if ($installer->tableExists($installer->getTable('ecosystem/queue_source'))) {
    $installer->getConnection()
        ->modifyColumn($this->getTable('ecosystem/queue_source'), 'entity_type', Varien_Db_Ddl_Table::TYPE_VARCHAR . "(255)")
        ->modifyColumn($this->getTable('ecosystem/queue_source'), 'entity_identifier', Varien_Db_Ddl_Table::TYPE_VARCHAR . "(255)")
        ->addIndex(
            $installer->getTable('ecosystem/queue_source'), 
            $installer->getIdxName(
                'ecosystem/queue_source', 
                array('entity_type', 'entity_identifier', 'target_id'), 
                Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('entity_type', 'entity_identifier', 'target_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}

if ($installer->tableExists($installer->getTable('ecosystem/queue_target'))) {
    $installer->getConnection()
        ->modifyColumn($this->getTable('ecosystem/queue_target'), 'entity_type', Varien_Db_Ddl_Table::TYPE_VARCHAR . "(255)")
        ->modifyColumn($this->getTable('ecosystem/queue_target'), 'entity_identifier', Varien_Db_Ddl_Table::TYPE_VARCHAR . "(255)")
        ->addIndex(
            $installer->getTable('ecosystem/queue_target'), 
            $installer->getIdxName(
                'ecosystem/queue_target', 
                array('entity_type', 'entity_identifier', 'source_id'), 
                Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('entity_type', 'entity_identifier', 'source_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}

$installer->endSetup();