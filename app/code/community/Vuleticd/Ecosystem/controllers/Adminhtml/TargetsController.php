<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Adminhtml_TargetsController extends Mage_Adminhtml_Controller_Action
{

    protected function _initTarget($idFieldName = 'id')
    {
        $id = (int)$this->getRequest()->getParam($idFieldName);
        $model = Mage::getModel('ecosystem/targets');
        if ($id) {
            $model->load($id);
        }
        if (!Mage::registry('target_item')) {
            Mage::register('target_item', $model);
        }
        return $model;
    }

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('ecosystem/targets');
        return $this;
    }

    public function indexAction()
    {
        $this->_title(Mage::helper('ecosystem')->__('Manage Targets'));
        $this->_initAction();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this -> _forward('edit');
    }

    public function editAction()
    {
        $this->_title(Mage::helper('ecosystem')->__('Manage Target'));

        $model = Mage::getModel('ecosystem/targets');

        // 2. if exists id, check it and load data
        $targetId = $this->getRequest()->getParam('id');
        if ($targetId) {
            $model->load($targetId);

            if (!$model->getId()) {
                $this->_getSession()->addError(Mage::helper('ecosystem')->__('Target does not exist.'));
                return $this->_redirect('*/*/');
            }

            $this->_title($model->getName());
            $breadCrumb = Mage::helper('ecosystem')->__('Edit Target');
        } else {
            $this->_title(Mage::helper('ecosystem')->__('New Target'));
            $breadCrumb = Mage::helper('ecosystem')->__('New Target');
        }

        $this->_initAction()->_addBreadcrumb($breadCrumb, $breadCrumb);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        // Register model to use later in blocks
        Mage::register('target_item', $model);
        $this->renderLayout();
    }

    public function saveAction()
    {
        $redirectPath = '*/*';
        $redirectParams = array();

        // check if data sent
        $data = $this->getRequest()->getPost();
        if ($data) {
            $model = Mage::getModel('ecosystem/targets');

            $targetId = $this->getRequest()->getParam('id');
            if ($targetId) {
                $model->load($targetId);
            }

            $model->setData($data);

            try {
                $hasError = false;

                // VALIDATION HERE
                // check base url format
                // check API connectivity
                // check if source can access target ecosystem.push API method, with supplied access details

                $model->save();
                $this->_getSession()->addSuccess(Mage::helper('ecosystem')->__('The target has been saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $redirectPath = '*/*/edit';
                    $redirectParams = array('id' => $model->getId());
                }
            } catch (Mage_Core_Exception $e) {
                $hasError = true;
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $hasError = true;
                $this->_getSession()->addException($e, 
                    Mage::helper('ecosystem')->__('An error occurred while saving the target.'));
            }

            if ($hasError) {
                $this->_getSession()->setFormData($data);
                $redirectPath = '*/*/edit';
                $redirectParams = array('id' => $targetId);
            }
        }

        $this->_redirect($redirectPath, $redirectParams);
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $title = "";
            try {
                $model = Mage::getModel('ecosystem/targets');
                $model->load($id);
                $title = $model->getName();
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('ecosystem')->__('The target has been deleted.'));
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ecosystem')->__('Unable to find target to delete.'));
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $targets= $this->getRequest()->getParam('target');
        if (!is_array($targets)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($targets as $target) {
                    $model = Mage::getModel('ecosystem/targets')->load($target);
                    $model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($targets)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this -> _redirect('*/*/index');
    }

    public function massEnableAction()
    {
        $targets= $this->getRequest()->getParam('target');
        if (!is_array($targets)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($targets as $target) {
                    $model = Mage::getModel('ecosystem/targets')->load($target);
                    $model->setEnabled(1);
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully enabled', count($targets)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this -> _redirect('*/*/index');
    }

    public function massDisableAction()
    {
        $targets= $this->getRequest()->getParam('target');
        if (!is_array($targets)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($targets as $target) {
                    $model = Mage::getModel('ecosystem/targets')->load($target);
                    $model->setEnabled(0);
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully enabled', count($targets)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this -> _redirect('*/*/index');
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function queueJobsGridAction()
    {
        $id = $this->getRequest()->getParam('id');
        $jobId = $this->getRequest()->getParam('process');
        if ($jobId) {
            $this->_runJobAction($jobId);
        }
        $model = $this->_initTarget('id');

        if (!$model->getId() && $id) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ecosystem')->__('This target no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _runJobAction($jobId)
    {
        $helper = Mage::helper('ecosystem');
        if ($jobId) {
            try {
                $job = Mage::getModel('ecosystem/job', array('direction' => 'push', 'id' => $jobId));
                if (!$job->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError($helper->__('Job does not exist.'));
                }

                if ($job->getEntityType()) {
                    $helper->debug('RUN::START ' . $job->getEntityType() . '::' . $job->getEntityIdentifier() . ' to Target "' . $job->getTargetId() . '".');
                    $profile = Mage::getModel('ecosystem/profile')->init($job);
                    $profile->push(true);
                    $helper->debug('RUN::FINISH ' . $job->getEntityType() . '::' . $job->getEntityIdentifier() . ' to Target "' . $job->getTargetId() . '".');
                }
                $job->delete();
            } catch (Exception $e) {
                $helper->debug($e->getMessage(), Zend_Log::ERR);
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError($helper->__('Job ID missing.'));
        }
    }

    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName())
        {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('ecosystem/targets/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('ecosystem/targets/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('ecosystem/targets');
                break;
        }
    }
}
