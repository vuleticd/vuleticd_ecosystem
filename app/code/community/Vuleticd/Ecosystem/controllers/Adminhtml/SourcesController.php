<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Vuleticd
 * @package     Vuleticd_Ecosystem
 * @copyright   Copyright (c) 2013 Vuletic Dragan (http://www.vuleticd.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Vuleticd_Ecosystem_Adminhtml_SourcesController extends Mage_Adminhtml_Controller_Action
{

    protected function _initSource($idFieldName = 'id')
    {
        $id = (int)$this->getRequest()->getParam($idFieldName);
        $model = Mage::getModel('ecosystem/sources');
        if ($id) {
            $model->load($id);
        }
        if (!Mage::registry('source_item')) {
            Mage::register('source_item', $model);
        }
        return $model;
    }

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('ecosystem/sources');
        return $this;
    }

    public function indexAction()
    {
    	$this->_title(Mage::helper('ecosystem')->__('Manage Sources'));
        $this->_initAction();
        $this->renderLayout();
    }

    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_title(Mage::helper('ecosystem')->__('Manage Source'));

        $model = Mage::getModel('ecosystem/sources');

        // 2. if exists id, check it and load data
        $sourceId = $this->getRequest()->getParam('id');
        if ($sourceId) {
            $model->load($sourceId);

            if (!$model->getId()) {
                $this->_getSession()->addError(Mage::helper('ecosystem')->__('Source does not exist.'));
                return $this->_redirect('*/*/');
            }

            $this->_title($model->getName());
            $breadCrumb = Mage::helper('ecosystem')->__('Edit Source');
        } else {
            $this->_title(Mage::helper('ecosystem')->__('New Source'));
            $breadCrumb = Mage::helper('ecosystem')->__('New Source');
        }

        $this->_initAction()->_addBreadcrumb($breadCrumb, $breadCrumb);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        // Register model to use later in blocks
        Mage::register('source_item', $model);
        $this->renderLayout();
    }

    public function saveAction()
    {
        $redirectPath = '*/*';
        $redirectParams = array();

        // check if data sent
        $data = $this->getRequest()->getPost();
        if ($data) {
            $model = Mage::getModel('ecosystem/sources');

            $sourceId = $this->getRequest()->getParam('id');
            if ($sourceId) {
                $model->load($sourceId);
            }

            $model->setData($data);

            try {
                $hasError = false;

                // VALIDATION HERE
                // check base url format
                // check API connectivity

                $model->save();
                $this->_getSession()->addSuccess(Mage::helper('ecosystem')->__('The source has been saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $redirectPath = '*/*/edit';
                    $redirectParams = array('id' => $model->getId());
                }
            } catch (Mage_Core_Exception $e) {
                $hasError = true;
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $hasError = true;
                $this->_getSession()->addException($e, 
                    Mage::helper('ecosystem')->__('An error occurred while saving the source.'));
            }

            if ($hasError) {
                $this->_getSession()->setFormData($data);
                $redirectPath = '*/*/edit';
                $redirectParams = array('id' => $sourceId);
            }
        }

        $this->_redirect($redirectPath, $redirectParams);
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $title = "";
            try {
                $model = Mage::getModel('ecosystem/sources');
                $model->load($id);
                $title = $model->getName();
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('ecosystem')->__('The source has been deleted.'));
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ecosystem')->__('Unable to find a source to delete.'));
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $sources= $this->getRequest()->getParam('source');
        if (!is_array($sources)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($sources as $source) {
                    $model = Mage::getModel('ecosystem/sources')->load($source);
                    $model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($sources)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this -> _redirect('*/*/index');
    }

    public function massEnableAction()
    {
        $sources= $this->getRequest()->getParam('source');
        if (!is_array($sources)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($sources as $source) {
                    $model = Mage::getModel('ecosystem/sources')->load($source);
                    $model->setEnabled(1);
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully enabled', count($sources)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this -> _redirect('*/*/index');
    }

    public function massDisableAction()
    {
        $sources= $this->getRequest()->getParam('source');
        if (!is_array($sources)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($sources as $source) {
                    $model = Mage::getModel('ecosystem/sources')->load($source);
                    $model->setEnabled(0);
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully disabled', count($sources)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this -> _redirect('*/*/index');
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function queueJobsGridAction()
    {
        $id = $this->getRequest()->getParam('id');
        $jobId = $this->getRequest()->getParam('process');
        if ($jobId) {
            $this->_runJobAction($jobId);
        }

        $model = $this->_initSource('id');

        if (!$model->getId() && $id) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ecosystem')->__('This source no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _runJobAction($jobId)
    {
        $helper = Mage::helper('ecosystem');
        if ($jobId) {
            try {
                $job = Mage::getModel('ecosystem/job', array('direction' => 'pull', 'id' => $jobId));

                if (!$job->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError($helper->__('Job does not exist.'));
                }

                if ($job->getEntityType()) {
                    $helper->debug('RUN::START ' . $job->getEntityType() . '::' . $job->getEntityIdentifier() . ' from Source "' . $job->getSourceId() . '".');
                    $profile = Mage::getModel('ecosystem/profile')->load($job);
                    $profile->pull(true);
                    $helper->debug('RUN::FINISH ' . $job->getEntityType() . '::' . $job->getEntityIdentifier() . ' from Source "' . $job->getSourceId() . '".');
                }

                $job->delete();
            } catch (Exception $e) {
                $helper->debug($e->getMessage(), Zend_Log::ERR);
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError($helper->__('Job ID missing.'));
        }
    }

    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName())
        {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('ecosystem/sources/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('ecosystem/sources/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('ecosystem/sources');
                break;
        }
    }
}
