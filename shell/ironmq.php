<?php

require 'abstract.php';

class Vuleticd_Ecosystem_IronMQ extends Mage_Shell_Abstract
{
    public function run()
    {
        if ($this->getArg('daemon')) {
            $instance = Mage::getModel('ecosystem/traffic_ironmq');
            $ironmq = $instance->getIronClient();
            while (true) {
                $message = $ironmq->getMessage(Mage::helper('ecosystem')->getQueueName());
                if (!$message) {
                    sleep(1);
                    continue;
                }

                $instance->args = $message;
                try {
                    $instance->perform();
                } catch(Exception $e) {
                    $instance->fail($e);
                }
            }
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php resque.php -- [options]
  --daemon Start IronMQ Worker and fork into the background
  -h                    Short alias for help
  help                  This help

USAGE;
    }
}

$shell = new Vuleticd_Ecosystem_IronMQ();
$shell->run();