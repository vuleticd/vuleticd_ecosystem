<?php

require 'abstract.php';

class Vuleticd_Ecosystem_Resque extends Mage_Shell_Abstract
{
    public function run()
    {
        if ($this->getArg('daemon')) {
            $QUEUE = Mage::helper('ecosystem')->getQueueName();
            $host = (string)Mage::getConfig()->getNode('ecosystem/redis/host');
            $port = (string)Mage::getConfig()->getNode('ecosystem/redis/port');
            $db = (string)Mage::getConfig()->getNode('ecosystem/redis/db');
            Resque::setBackend($host.':'.$port, $db);
        
            $logLevel = false;
            $LOGGING = getenv('LOGGING');
            $VERBOSE = getenv('VERBOSE');
            $VVERBOSE = getenv('VVERBOSE');
            if(!empty($LOGGING) || !empty($VERBOSE)) {
                $logLevel = true;
            }
            else if(!empty($VVERBOSE)) {
                $logLevel = true;
            }

            // See if the APP_INCLUDE containes a logger object,
            // If none exists, fallback to internal logger
            if (!isset($logger) || !is_object($logger)) {
                $logger = new Resque_Log($logLevel);
            }

            $BLOCKING = getenv('BLOCKING') !== FALSE;

            $interval = 5;
            $INTERVAL = getenv('INTERVAL');
            if(!empty($INTERVAL)) {
                $interval = $INTERVAL;
            }

            $count = 1;
            $COUNT = getenv('COUNT');
            if(!empty($COUNT) && $COUNT > 1) {
                $count = $COUNT;
            }

            $PREFIX = getenv('PREFIX');
            if(!empty($PREFIX)) {
                $logger->log(Psr\Log\LogLevel::INFO, 'Prefix set to {prefix}', array('prefix' => $PREFIX));
                Resque_Redis::prefix($PREFIX);
            }

            if($count > 1) {
                for($i = 0; $i < $count; ++$i) {
                    $pid = Resque::fork();
                    if($pid == -1) {
                        $logger->log(Psr\Log\LogLevel::EMERGENCY, 'Could not fork worker {count}', array('count' => $i));
                        die();
                    }
                    // Child, start the worker
                    else if(!$pid) {
                        $queues = explode(',', $QUEUE);
                        $worker = new Resque_Worker($queues);
                        $worker->setLogger($logger);
                        $logger->log(Psr\Log\LogLevel::NOTICE, 'Starting worker {worker}', array('worker' => $worker));
                        $worker->work($interval, $BLOCKING);
                        break;
                    }
                }
            }
            // Start a single worker
            else {
                $queues = explode(',', $QUEUE);
                $worker = new Resque_Worker($queues);
                $worker->setLogger($logger);
                $PIDFILE = getenv('PIDFILE');
                if ($PIDFILE) {
                    file_put_contents($PIDFILE, getmypid()) or
                        die('Could not write PID information to ' . $PIDFILE);
                }

                $logger->log(Psr\Log\LogLevel::NOTICE, 'Starting worker {worker}', array('worker' => $worker));
                $worker->work($interval, $BLOCKING);
            }
        } else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php resque.php -- [options]
  --daemon Start Resque and fork into the background
  -h                    Short alias for help
  help                  This help

USAGE;
    }
}

$shell = new Vuleticd_Ecosystem_Resque();
$shell->run();