# Magento Ecosystem

Communication protocol for data exchange between Magento websites, designed with stability and flexibility in mind. 

## Protocol

* When Magento entity is changed, source website will create the job and send it to target website. 
* Target website will receive the job and request full entity data from source, executing pre-defined API endpoint on source website.
* Target website will then process received entity data. 

Sending and receiving of jobs is defined by traffic adapters. MySQL queued, Realtime and Resque adapters are available but custom traffic adapter can be used as well.

Unique entity attribute, pre-defined API method and process of saving received entity data is defined by entity adapters. Example entity adapters are available for Magento Websites, store groups and store views.
 
## Job

Job is defined as collection of:
 
* Changed entity model class. This is always Magento out of the box entity model, even if it was rewritten on source and/or target website. Ecosystem is smart enough to detect rewrites and to load correct model class.
* Original and current value of unique entity attribute shared between source and target website. For example, SKU for products, or code for Magento store views.
* Base URL of source website



## Traffic adapters

### MySQL queue

Source and Target have queuing MySQL tables where jobs are queued before processing. 
Magento cron job is processing jobs from queues, pulling the jobs one by one, every minute. 
Processed job is sent to ecosystem.push API method by source website.
Processed job executes pre-defined API endpoint by target website.

### Realtime

Job is directly sent to ecosystem.push API method by source website.
This will trigger instant execution of pre-defined API endpoint by target website.

### Resque

Job is directly sent to resque and saved in queue named as Base URL of target website. 
Each target website has resque worker daemon running, receiving jobs from it's reserved queue
This will trigger instant execution of pre-defined API endpoint by target website.

### IronMQ

Job is directly sent to IronMQ and saved in queue named as Base URL of target website. 
Each target website has IronMQ worker daemon running, receiving jobs from it's reserved queue
This will trigger instant execution of pre-defined API endpoint by target website.

### Custom 

This can be anything describing how jobs are exchanged between source and target websites.

* FTP server and jobs as CSV files
* Some other MQ
* ...

To create custom traffic adapter, define adapter configuration in your config.xml file

```xml
<ecosystem>
    <traffic>
        <adapter>
            <custommq>Custom_Extension_Model_Traffic_Adapter_Mq</custommq>
        </adapter>
    </traffic>
</ecosystem>
```

Custom traffic adapter class must extend Vuleticd_Ecosystem_Model_Traffic_Adapter_Abstract and optionally implement methods push and/or pull.
Each method has 3 arguments $job, $remote, $entityAdapter.

Abstract push will send job to target website ($remote), by calling ecosystem.push API method.
Resque traffic adapter override this with custom push method which sends $job to Resque

```php
public function push($job, $remote, $entityAdapter)
{
    // send to resque, skip ecosystem.push API completely
    $host = (string)Mage::getConfig()->getNode('ecosystem/redis/host');
    $port = (string)Mage::getConfig()->getNode('ecosystem/redis/port');
    $db = (string)Mage::getConfig()->getNode('ecosystem/redis/db');
    Resque::setBackend($host.':'.$port, $db);
    $args = array(
            'type' => $job->getEntityType(),
            'identifier' => $entityAdapter->getIdentifier(),
            'url'   => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
        );
    $jobId = Resque::enqueue(rtrim($remote->getBaseUrl(), '/'), get_class($this), $args);
}
```

Abstract pull method will call pre-defined API endopint, on source website ($remote) and process returned results.
MySQL Queue traffic adapter override this with custom pull method which sends job to MySQL queue, if not forced.

```php
public function pull($job, $remote, $entityAdapter)
{
    if ($this->getForced()) {
        // instantly trigger execution of pre-defined API endpoint
        parent::pull($job, $remote, $entityAdapter);
    } else {
        // save in queue first, then force with cron job
        $model = Mage::getModel('ecosystem/queue_target');
        $model->setSourceId($remote->getId());
        $model->setEntityIdentifier($job->getEntityIdentifier());
        $model->setEntityType($job->getEntityType());
        $model->save();
    }
}
```

## Entity adapters

### Websites

[https://bitbucket.org/vuleticd/vuleticd_ecosystemwebsites](https://bitbucket.org/vuleticd/vuleticd_ecosystemwebsites)

### Store Groups

Link to Store Groups extension repo

### Store Views

Link to Store Views extension repo

### Custom

Example of custom entity extension development

## Features

* Each website can send and/or receive to/from multiple sources
* Jobs are always sent and received one by one. Bulk processing is not supported.
* Jobs are sent only if change is detected and entity enabled. No change, no traffic, no load.
* Transfer adapter is configurable per entity, per source/target or globally.
* Ability to ignore, filter out entity attributes returned by pre-defined API endpoint. Usable in cases when endpoint is Magento out of the box API method like sales_order.info. 
* Ability to enable/disable entity globally and for every source/target.
* Ability to disable/enable sources/targets. 



## Resque setup

To enable Resque, you'll need Redis server accessible to all source and target websites.
Configuration of redis server is placed in module config.xml file. By default it is:

```xml
<ecosystem>
        <redis>
            <host>localhost</host>
            <port>6379</port>
            <db>10</db>                
        </redis>
</ecosystem>
```

You should reserve redis database, or even better redis instance, just for resque.
Each target website must have following resque worker daemon running **php shell/resque.php --daemon** to be able to process the queue.

## IronMQ setup

To enable IronMQ, you'll need IronMQ token and project_id.
Configuration of IronMQ is placed in module config.xml file. By default it is:

```xml
<ecosystem>
        <ironmq>
            <token>XXXXXXX</token>
            <project>XXXXXXX</project>               
        </ironmq>
</ecosystem>
```

Each target website must have following worker daemon running **php shell/ironmq.php --daemon** to be able to process the queue.

## Why did we create this, and how can I use this for my E-commerce business ?

There is often business need for Magento websites to communicate with each other and exchange data.
Usual scenarios include:

* You need to migrate data from your old Magento website into newly developed Magento website.
* You have to share your product catalog, or part of your products catalog with other websites. ( franchisees, affiliates,... ) 
* You need to receive data from multiple websites (orders from affiliates, cms blocks as ads from partners,...)

This communication was usually achieved with all kinds of export/import, data warehouse functionalities. 
There are few big problems with those approaches:

* Data is duplicated, either in flat files like CSV, or in some external data warehouse database. In any case, it's not necessary to duplicate the data, which already exists inside source database. Duplication means that constant synchronization is required, and if fails it will result in invalid state of external storage.
* External storage becomes single point of failiure of entire system. Invalid external storage may become disaster for all websites subscribed to it. For example, catalog may become invalid or missing on target websites when new arrivals are launched.
* It becomes very hard to relise that something is wrong. Typically, it will be reported by target website, too late to react.
* The logic, for each entity exchange, is usually hardcoded making it very hard to debug or introduce any changes. The testing of new changes becomes very long and costly process.
* Usually, both source and target websites will need some level of control over what is processed and when. Maybe even the preview before processing. Since actual data is in some external storage, these type of controls become very difficult to implement and usually require unneeded code to be executed in order to make them possible.

The point is that there is no need for this. Duplication of data, external storage, hardcoding of logic are usually not requirements. Requirements are usually simply to make these websites aware of each other and enable exchange of data between them.

Well, after seeing some pretty big investments with pretty scary results, we've decided to design and develop Magento Ecosystem.
Hopefully, it will be helpful to some businesses.

## Contributing

1. Before starting work on a new contribution, take a moment and search the issues and commits for similar proposals.
2. Fork the Magento Ecosystem repository into your account according to BitBucket's Fork a Repo.
3. Make your changes. We also recommend you test your code before contributing.
4. Once ready to commit your changes, create a pull request according to BitBucket's Create a Pull Request.
5. Once received, the Magento Ecosystem development team will review your contribution and if approved, will pull your request to the appropriate branch.

Note: You must agree to Contributor License Agreement before pulling any requests. You only need to sign the agreement once.